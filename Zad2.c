/* Napisać program, który zrobi to samo co program pierwszy, ale zapisze zaczytane
dane oraz wyniki do struktury. Program skorzysta z modułów do statystyk opisowych.
Program sprawdzi, czy w pliku są już dopisane statystyki i jeśli ich nie ma, dopisze je.
W modułach mają znaleźć się tylko funkcje/procedury dla statystyk, ewentualnie definicja struktury.*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

float druktab(float *tab, int w);
float average(float *tab, int w, int k);
float mediantab(float *tab, int w, int k);
float odchylenietab(float *tab, int w, int k, float srednia);
float dopisz(FILE *fp, float s0, float m0, float o0, float s1, float m1, float o1, float s2, float m2, float o2);

struct Statystyki   //strutktura dla statystyk
{
    float average;
    float median;
    float deviation;
};

//tutaj plikdotab też w mainie

int main()
{
    FILE *fp;   //18 - 47 kod wczytujący tablice z pliku do trzech dynamicznych tablic floatów
    int x = 50;
    if ((fp = fopen("plik.txt", "r")) == NULL)
    {
        printf("Error");
        exit(1);
    }
    char p[10] = "";
    char q[10] = "";
    char r[10] = "";
    char s[10] = "";
    fscanf(fp, "%s\t%s\t\t%s\t\t%s\n", p, q, r, s); //zczytanie nagłówka
    float *tab0;
    float *tab1;
    float *tab2;
    int i = 0;
    tab0 = (float*)malloc(x*sizeof(float));    //zaalokowanie tablic dynamicznych
    tab1 = (float*)malloc(x*sizeof(float));
    tab2 = (float*)malloc(x*sizeof(float));
    float t = 0.0;
    float u = 0.0;
    float v = 0.0;
    for (i = 0; i < x; i++) //pętla czytająca każdy wiersz
    {
        fscanf(fp, "%s\t%f\t\t%f\t%f\n", s, &t, &u, &v);
        tab0[i] = t;
        tab1[i] = u;
        tab2[i] = v;
    }
    
    int k = 0;
    struct Statystyki A;
    //druktab(tab0, x);   //funkcja dla pokazania tablicy
    A.average = average(tab0, x, k);    //funkcja dla sredniej kolumny
    A.median = mediantab(tab0, x, k); //funkcja dla mediany
    A.deviation = odchylenietab(tab0, x, k, A.average);    //funkcja dla odchylenia

    k = 1;
    struct Statystyki B;
    //druktab(tab0, x);   //funkcja dla pokazania tablicy
    B.average = average(tab1, x, k);    //funkcja dla sredniej kolumny
    B.median = mediantab(tab1, x, k); //funkcja dla mediany
    B.deviation = odchylenietab(tab1, x, k, B.average);    //funkcja dla odchylenia

    k = 2;
    struct Statystyki C;
    //druktab(tab0, x);   //funkcja dla pokazania tablicy
    C.average = average(tab2, x, k);    //funkcja dla sredniej kolumny
    C.median = mediantab(tab2, x, k); //funkcja dla mediany
    C.deviation = odchylenietab(tab2, x, k, C.average);    //funkcja dla odchylenia

    dopisz("plik.txt", A.average, A.median, A.deviation, B.average, B.median, B.deviation, C.average, C.median, C.deviation);

    return 0;
}

float druktab(float *tab, int w) //procedrura dla pokazania tablicy
{
    int i = 0;
    for (i = 0; i < w; i++)
    {
        printf("%d. %f\n", i + 1, tab[i]);
    }
}

float average(float *tab, int x, int k)  //funkcja dla sredniej
{
    int i = 0;
    float srednia = 0.0;
    for (i = 0; i < x; i++)
    {
        srednia = srednia + tab[i];
    }
    srednia = srednia / i;

    char p[4] = "";
    if (k == 0)
    {
        strcpy(p, "X");
    }
    else if (k == 1)
    {
        strcpy(p, "Y");
    }
    else if (k == 2)
    {
        strcpy(p, "RHO");
    }

    printf("Srednia dla calej kolumny %s wynosi: %f\n", p, srednia);  //informacja o średniej
    return srednia;
}

float mediantab(float *tab, int x, int k)  //funkcja liczenia mediany
{
    float temp = 0.0;   //sortowanie tablicy 110 - 122
    for (int i = 0; i < x; i++)
    {     
        for (int j = i+1; j < x; j++) 
        {     
           if(tab[i] > tab[j]) 
           {    
               temp = tab[i];    
               tab[i] = tab[j];    
               tab[j] = temp;    
           }     
        }     
    }
    float mediana = 0.0;
    int y = x / 2;  //indeks polowy tablicy
    mediana = tab[y];   //wartosc tego indeksu

    char p[4] = "";
    if (k == 0)
    {
        strcpy(p, "X");
    }
    else if (k == 1)
    {
        strcpy(p, "Y");
    }
    else if (k == 2)
    {
        strcpy(p, "RHO");
    }

    printf("Mediana kolumny %s wynosi: %f\n", p, mediana);
    return mediana;
}

float odchylenietab(float *tab, int x, int k, float srednia)   //funkcja dla odchylenia
{
    int i = 0;
    float temp = 0.0;
    for (i = 0; i < x; i++)
    {
        temp = temp + pow(tab[i] - srednia, 2);
    }
    temp = temp / x;
    temp = sqrt(temp);

    char p[4] = "";
    if (k == 0)
    {
        strcpy(p, "X");
    }
    else if (k == 1)
    {
        strcpy(p, "Y");
    }
    else if (k == 2)
    {
        strcpy(p, "RHO");
    }

    printf("Odchylenie standardowe kolumny %s wynosi: %f\n\n", p, temp);
    return temp;
}

float dopisz(FILE *fp, float s0, float m0, float o0, float s1, float m1, float o1, float s2, float m2, float o2)    //estetycznie dodaje statystyki do pliku
{
    if ((fp = fopen(fp, "a")) == NULL)
    {
        printf("Error");
        exit(1);
    }
    fprintf(fp, "\n\nSr\t%.5f\t\t%.5f\t%.3f\nMe\t%.5f\t\t%.5f\t%.3f\nOd\t%.5f\t\t%.5f\t%.3f", s0, s1, s2, m0, m1, m2, o0, o1, o2);
    fclose(fp);
    return;
}